(function() {
'use strict';

  angular
    .module('TrackingModule')
    .factory('TrackingService', TrackingService);

  TrackingService.$inject = ['$http','urlsConst','AuthService'];
  function TrackingService($http, urlsConst, AuthService) {
    let headers = AuthService.getHeaders();
    let apiUrl = urlsConst.API;
    let service = {
      getLocationByDeviceId: getLocationByDeviceId
    };
    
    return service;

    function getLocationByDeviceId(deviceId) {
      let url = apiUrl + '/api/locations/device?id=' + deviceId;
      return $http({
        method: 'GET',
        url: url,
        headers: headers
      });
    }

  }
})();