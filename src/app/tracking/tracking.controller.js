(function() {
'use strict';

  angular
    .module('TrackingModule', [])
    .controller('TrackingController', TrackingController);

  TrackingController.$inject = ['DevicesService','ConfigService','TrackingService','Notification','$interval','NgTableParams','ParseHelperService'];
  function TrackingController(DevicesService, ConfigService, TrackingService, Notification, $interval, NgTableParams, ParseHelperService) {
    var vm = this;
    const REFRESH_INTERVAL = 2000;

    vm.selectedDevice = null;
    vm.isTracking = false;
    vm.deviceData = null;
    vm.mapSettings = null;
    vm.location = null;
    vm.trackingInfoData = [];
    vm.trackingInfoTableHeaders = [{ name: 'latitude'}, {name: 'longitude'}, {name: 'speed'}, {name: 'timestamp'}]

    vm.startTrackingClick = startTrackingClick;
    vm.stopTrackingClick = stopTrackingClick;
    vm.trackingInfoTableParams = null;

    function startTrackingClick() {
      setIntervalRefresh();
      getTracking();
    }

    function getTracking() {
      TrackingService.getLocationByDeviceId(vm.selectedDevice.id)
        .then(getLocationSuccess)
        .catch(() => { Notification.error('Error retiving data'); });

      function getLocationSuccess(response) {
        if (response.data) {
          let location = response.data;
          location.timestamp = ParseHelperService.getDateTimeFromString(location.timestamp);
          let position = [location.latitude.replace(',','.'), location.longitude.replace(',','.')]
          vm.mapSettings = {
            center: position,
            zoom: 16
          };

          if (isChanged(response.data)) {
            vm.deviceData = {
              position: position,
              name: response.data.name
            };
            vm.trackingInfoData.push(location);
            vm.location = location;
            vm.isTracking = true;  
            initTrackingInfoTableParams();
          }
        }
      }
    }

    function isChanged(newLocation) {
      let result = true;
      if (vm.location) {
        result = vm.location.latitude !== newLocation.latitude && vm.location.longitude !== newLocation.longitude;
      }
      return result;
    }
    
    function initTrackingInfoTableParams() {
      let tableParams = getTableParams();
      let tableSettings = getTableSettings();

      vm.trackingInfoTableParams = new NgTableParams(tableParams, tableSettings);

      function getTableSettings() {
        let settings = {
          dataset: vm.trackingInfoData
        };
        return settings;
      }

      function getTableParams() {
        let params = {
          count: 10,
          sorting: {timestamp: 'desc'}
        };
        return params;
      }
    }

    function stopTrackingClick() {
      vm.isTracking = false;
      vm.deviceData = null;
      vm.mapSettings = null;
      vm.location = null;
      vm.trackingInfoData = [];
      cancelIntervalRefresh();
    }

    function setIntervalRefresh() {
      vm.refresh = $interval(getTracking, REFRESH_INTERVAL);
    }

    function cancelIntervalRefresh() {
      $interval.cancel(vm.refresh);
    }
  }
})();