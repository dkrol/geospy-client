(function() {
  'use strict';

  angular
    .module('GeoSpy')
    .config(config);

  /** @ngInject */
  config.$inject = ['$mdDateLocaleProvider','$logProvider','toastrConfig','NotificationProvider'];
  function config($mdDateLocaleProvider, $logProvider, toastrConfig, NotificationProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastrConfig.allowHtml = true;
    toastrConfig.timeOut = 3000;
    toastrConfig.positionClass = 'toast-top-right';
    toastrConfig.preventDuplicates = true;
    toastrConfig.progressBar = true;

    // Set callendar options
    $mdDateLocaleProvider.months = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];
    $mdDateLocaleProvider.shortMonths = ['Sty', 'Lut', 'Marz', 'Kwie', 'Maj', 'Czer', 'Lip', 'Sier', 'Wrze', 'Paź', 'List', 'Grudź'];
    $mdDateLocaleProvider.days = ['Niedziela','Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota'];
    $mdDateLocaleProvider.shortDays = ['N','P','W','Ś','C','P','S'];
  
    // Notification configuration
    NotificationProvider.setOptions({ 
      delay: 5000, 
      startTop: 20, 
      startRight: 10, 
      verticalSpacing: 20, 
      horizontalSpacing: 20, 
      positionX: 'right', 
      positionY: 'bottom' 
    });

  }
})();
