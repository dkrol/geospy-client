(function() {
'use strict';

  angular
    .module('MainModule')
    .controller('MainController', MainController);

  MainController.$inject = ['$mdSidenav','$state','ConfigService','$window'];
  function MainController($mdSidenav, $state, ConfigService, $window) {
    var vm = this;
    
    vm.menuItems = [];

    vm.toggleMenu = toggleMenu;
    vm.menuHeaderClick = menuHeaderClick;
    vm.menuItemClick = menuItemClick;
    vm.logoutClick = logoutClick;

    initMenuItems();

    function initMenuItems() {
      vm.menuItems.push({
        header: 'Dashboard',
        uisref: 'dashboard',
        iconClass: 'fa fa-home',
        isSelected: true
      });

      vm.menuItems.push({
        header: 'Devices',
        uisref: 'devices',
        iconClass: 'fa fa-tablet',
        isSelected: false
      });

      vm.menuItems.push({
        header: 'Tracking',
        uisref: 'tracking',
        iconClass: 'fa fa-map-marker',
        isSelected: false
      });

      vm.menuItems.push({
        header: 'Location',
        uisref: 'location',
        iconClass: 'fa fa-map',
        isSelected: false
      });

      // vm.menuItems.push({
      //   header: 'Users',
      //   uisref: 'users',
      //   iconClass: 'fa fa-user',
      //   isSelected: false
      // });
    }
    
    function toggleMenu() {
      $mdSidenav('left').toggle();
    }

    function menuHeaderClick() {
      cleanSelectedItem();
      vm.menuItems[0].isSelected = true;
      $state.go('dashboard');
    }

    function menuItemClick(index) {
      cleanSelectedItem();
      vm.menuItems[index].isSelected = true;
    }

    function logoutClick() {
      ConfigService.clearProfile();
      $window.location.href = '/';
    }

    function cleanSelectedItem() {
      for (var i = 0, length = vm.menuItems.length; i < length; i++) {
        vm.menuItems[i].isSelected = false;
      }
    }
  }
})();