(function() {
  'use strict';

  angular
    .module('MainModule', [])
    .directive('main', main);

  function main() {
    var directive = {
        templateUrl: 'app/main/main.view.html',
        controller: 'MainController',
        controllerAs: 'mainCtrl',
        restrict: 'E',
        scope: {
        }
    };
    return directive;
  }

})();