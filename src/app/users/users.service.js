(function () {
  'use strict';

  angular
    .module('GeoSpy')
    .factory('UsersService', UsersService);

  UsersService.$inject = ['$http'];
  function UsersService($http) {
    //var url = 'http://192.168.0.101:9810/api/users';
    //var url = 'http://localhost:28424/api/users';
    var url = '';
    var service = {
      getUsers: getUsers
    };

    return service;

    function getUsers() {
      return $http({
        method: 'GET',
        url: url
      });
    }

  }
})();