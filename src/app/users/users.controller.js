(function() {
'use strict';

  angular
    .module('UsersModule', [])
    .controller('UsersController', UsersController);

  UsersController.$inject = ['UsersService','Notification','$log'];
  function UsersController(UsersService, Notification) {
    var vm = this;
    vm.users = null;
    vm.originUsers = null;

    initTable();
    initUsers();

    notificationTest();

    function initTable() {
      vm.tableHeaders = [
        { name: "Id" },
        { name: "Login" },
        { name: "Mail" }
      ]
    }

    function initUsers() {
      UsersService.getUsers()
        .then(getUsersSuccess)
        .catch(getUsersError);

      function getUsersSuccess(response) {
        if (response.data) {
          vm.originUsers = angular.copy(response.data);
          vm.users = response.data;
          if (!vm.users || vm.users.lenght < 1) {
            Notification.primary('Brak danych');
          }
        }
      }

      function getUsersError(error) {
        Notification.primary('Błąd połączenia');
        console.log(error);
      }
    }

    function notificationTest() {
      // Notification.primary('Primary notification');
      // Notification('Primary notification');
      // Notification.success('Success notification');

      // Message with custom type
      Notification({message: 'Warning notification'}, 'warning');

      // With Title
      Notification({message: 'Primary notification', title: 'Primary notification'});

      // Message with custom delay
      // Notification.error({message: 'Error notification 1s', delay: 1000});

      // Change position notification
      // Notification.error({message: 'Error Bottom Right', positionY: 'bottom', positionX: 'right'});

      // Replace message
      // Notification.error({message: 'Error notification 1s', replaceMessage: true});
    }
  }
})();