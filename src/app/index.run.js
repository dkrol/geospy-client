(function() {
  'use strict';

  angular
    .module('GeoSpy')
    .run(runBlock);
    
  runBlock.$inject = ['$rootScope','$log','ConfigService','PermissionService','roleName'];  
  function runBlock($rootScope, $log, ConfigService, PermissionService, roleName) {

    $rootScope.$on('$destroy', onDestroy);
    $rootScope.roleName = roleName;

    let stateChangeStartVar = $rootScope.$on('$stateChangeStart', stateChangeStart);
    // let stateChangePermissionStartVar = $rootScope.$on('$stateChangePermissionStart', stateChangePermissionStart);
    // let stateChangePermissionAcceptedVar = $rootScope.$on('$stateChangePermissionAccepted', stateChangePermissionAccepted);
    // let stateChangePermissionDeniedVar = $rootScope.$on('$stateChangePermissionDenied', stateChangePermissionDenied);

    $log.debug('runBlock end');

    definePermission();

    function definePermission() {
      PermissionService.definePermission();
    }

    function stateChangeStart(event, toState) {
      console.log('STATE CHANGE START');
      if (toState.views && toState.views.bodyView) {
        $rootScope.title = toState.views.bodyView.data.title;
      } else if (toState.views && toState.views.loginView) {
        $rootScope.title = toState.views.loginView.data.title;
      }
    }

    // function stateChangePermissionStart(event, toState, toParams, options) {
    //   console.log('STATE CHANGE PERMISSION START');
    // }

    // function stateChangePermissionAccepted(event, toState, toParams, options) {
    //   console.log('STATE CHANGE PERMISSION ACCEPTED');
    // }

    // function stateChangePermissionDenied(event, toState, toParams, options) {
    //   console.log('STATE CHANGE PERMISSION DENIED');
    // }

    function onDestroy() {
      stateChangeStartVar();
      // stateChangePermissionStartVar();
      // stateChangePermissionAcceptedVar();
      // stateChangePermissionDeniedVar();
    }
  }

})();
