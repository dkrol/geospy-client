(function() {
'use strict';

  angular
    .module('DevicesModule', [])
    .controller('DevicesController', DevicesController);

  DevicesController.$inject = ['DevicesService','ConfigService','Notification','NgTableParams'];
  function DevicesController(DevicesService, ConfigService, Notification, NgTableParams) {
    var vm = this;

    vm.devices = null;
    vm.devicesTableParams = null;

    class Test {
      constructor() {

      }
    }


    initDevices();

    function initDevices() {
      let test = new Test();


      let user = ConfigService.getProfile();
      DevicesService.getDevicesByUser(user)
        .then(getDevicesSuccess)
        .catch(() => { Notification.error('Error retrieving data'); });

      function getDevicesSuccess(response) {
        if (response.data) {
          vm.devices = response.data;
          initDevicesTable();
        } else {
          Notification.warning('No data available');
        }
      }
    }

    function initDevicesTable() {
      let tableParams = getTableParams();
      let tableSettings = getTableSettings();

      vm.devicesTableParams = new NgTableParams(tableParams, tableSettings);

      function getTableSettings() {
        let settings = {
          dataset: vm.devices
        };
        return settings;
      }

      function getTableParams() {
        let params = {
          count: 10
        };
        return params;
      }
    }
  }
})();