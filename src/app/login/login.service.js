(function () {
  'use strict';

  angular
    .module('LoginModule')
    .factory('LoginService', LoginService);

  LoginService.$inject = ['$http', 'urlsConst'];
  function LoginService($http, urlsConst) {
    let apiUrl = urlsConst.API;
    return {
      register: register,
      login: login
    };

    function login(data) {
      let url = `${apiUrl}/jwt`;
      let headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

      return $http({
        method: 'POST',
        url: url,
        data: data,
        headers: headers,
        transformRequest: transformRequest
      });


      // return $http({
      //   method: 'POST',
      //   url: url,
      //   data: data,
      //   headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      //   transformRequest: transformRequest
      // });


    }

    function register(data) {
      let url = apiUrl + '/api/users';
      return $http({
        method: 'POST',
        url: url,
        data: data
      });
    }

    // function register(data) {   
    //   let url = apiUrl + '/api/Account/Register';
    //   return $http({
    //     method: 'POST',
    //     url: url,
    //     data: data
    //   });
    // }


    function transformRequest(obj) {
      var str = [];
      for(var p in obj) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
      
      return str.join("&");
    }
  }
})();