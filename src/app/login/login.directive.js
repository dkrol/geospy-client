(function() {
  'use strict';

  angular
    .module('LoginModule', [])
    .directive('login', login);

  function login() {
    var directive = {
        templateUrl: 'app/login/login.view.html',
        controller: 'LoginController',
        controllerAs: 'loginCtrl',
        restrict: 'E',
        scope: {
        }
    };
    return directive;
  }
})();