(function () {
  'use strict';

  angular
    .module('RegistrationModule', [])
    .controller('RegistrationController', RegistrationController);

  RegistrationController.$inject = ['LoginService', 'NotificationService', '$state'];
  function RegistrationController(LoginService, NotificationService, $state) {
    var vm = this;

    vm.userData = null;
    vm.registrationClick = registrationClick;
    vm.keyPress = keyPress;

    function registrationClick() {
      if (vm.userData !== null) {
        registerUser(vm.userData);
      } else {
        NotificationService.error('Form has not been filled');
      }
    }

    function registerUser(user) {
      LoginService.register(user)
        .then(registerSuccess)
        .catch(registerFailed);

      function registerSuccess(response) {
        if (response.data) {
          NotificationService.success('Registration successfull');
          $state.go('signin');
        }
      }

      function registerFailed(error) {
        NotificationService.showResponseErrors(error);
      }
    }

    function keyPress($event) {
      let keyCode = $event.which || $event.keyCode
      if (keyCode === 13) {
        registrationClick();
      }
    }

  }
})();