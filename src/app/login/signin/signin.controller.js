(function () {
  'use strict';

  angular
    .module('SigninModule', [])
    .controller('SigninController', SigninController);

  SigninController.$inject = ['LoginService', 'NotificationService', 'ConfigService', '$state', '$window'];
  function SigninController(LoginService, NotificationService, ConfigService, $state, $window) {
    var vm = this;

    vm.userData = null;
    vm.loginClick = loginClick;
    vm.keyPress = keyPress;

    // function registrationClick() {
    //   if (vm.userData !== null) {
    //     registerUser(vm.userData);
    //   } else {
    //     NotificationService.error('Form has not been filled');
    //   }
    // }

    function loginClick() {
      if (vm.userData !== null) {
        loginUser(vm.userData);
      } else {
        NotificationService.error('Form has not been filled');
      }
    }

    function loginUser(user) {
      LoginService.login(user)
        .then(loginSuccess)
        .catch(() => { NotificationService.error('Incorrect login or password'); });

      function loginSuccess(response) {
        if (response.data) {
          let currentUser = {
            userName: response.data.userName,
            token: response.data.access_token,
            isLogged: true
          };

          ConfigService.setProfile(currentUser);
          $window.location.href = '/';
        }
      }
    }

    function keyPress($event) {
      let keyCode = $event.which || $event.keyCode
      if (keyCode === 13) {
        loginClick();
      }
    }

  }
})();