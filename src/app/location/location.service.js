(function() {
'use strict';

  angular
    .module('LocationModule')
    .factory('LocationService', LocationService);

  LocationService.$inject = ['$http','urlsConst','AuthService'];
  function LocationService($http, urlsConst, AuthService) {
    let headers = AuthService.getHeaders();
    let apiUrl = urlsConst.API;
    let service = {
      getLocationsByDevice: getLocationsByDevice,
      getLocationsByDeviceIdAndDatetimeRange: getLocationsByDeviceIdAndDatetimeRange
    };
    
    return service;

    function getLocationsByDevice(data) {
      let url = apiUrl + '/api/locations/device';
      return $http({
        method: 'POST',
        url: url,
        data: data,
        headers: headers
      });
    }

    function getLocationsByDeviceIdAndDatetimeRange(data) {
      let url = apiUrl + '/api/locations/device?id=' + data.deviceId;
      return $http({
        method: 'POST',
        url: url,
        data: data.datetimeRange,
        headers: headers
      });
    }
  }
})();