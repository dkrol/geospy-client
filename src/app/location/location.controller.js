(function() {
'use strict';

  angular
    .module('LocationModule', [])
    .controller('LocationController', LocationController);

  LocationController.$inject = ['LocationService','DevicesService','ConfigService','Notification', 'ParseHelperService'];
  function LocationController(LocationService, DevicesService, ConfigService, Notification, ParseHelperService) {
    var vm = this;

    vm.selectedDevice = null;
    vm.locatons = null;
    vm.locationsPath = null;

    vm.locationsPathZoom = 2;
    vm.locationsPathCenter = '0, -180';

    vm.getLocationsForDevice = getLocationsForDevice;
    vm.searchButtonClick = searchButtonClick;

    function searchButtonClick() {
      if (vm.selectedDevice !== null && isDatetimeFromAndToSelected()) {
        let data = prepareData();
        getLocationsForDevice(data);
      } else {
        Notification.warning('Fill empty fields in filtering')
      }
    }

    function isDatetimeFromAndToSelected() {
      return vm.datetimeFrom.date && vm.datetimeFrom.time && vm.datetimeTo.date && vm.datetimeTo.time;
    }

    function prepareData() {
      let date = {
        deviceId: vm.selectedDevice.id,
        datetimeRange: {
           from: ParseHelperService.getStringFromDateTime(vm.datetimeFrom),
           to: ParseHelperService.getStringFromDateTime(vm.datetimeTo)
        }
      };
      return date;
    }

    function getLocationsForDevice(data) {
      LocationService.getLocationsByDeviceIdAndDatetimeRange(data)
        .then(getLocationsSuccess)
        .catch(() => { Notification.error('Error retrieving data'); });

      function getLocationsSuccess(response) {
        if (response.data && response.data.length > 0) {
          vm.locations = response.data;
          initLocationsPath();
        } else {
          Notification.warning('No data available');
        }
      }
    }

    function initLocationsPath() {
      vm.locationsPath = [];
      if (vm.locations !== null) {
        for (let i = 0, length = vm.locations.length; i < length; i++) {
          let location = vm.locations[i];
          let position = [location.latitude.replace(',','.'), location.longitude.replace(',','.')];
          vm.locationsPath.push(position);
        }
        setLocationsPathSettings();
      }
    }

    function setLocationsPathSettings() {
      vm.locationsPathZoom = 17;
      vm.locationsPathCenter = vm.locationsPath[vm.locationsPath.length - 1];
    }
  }
})();