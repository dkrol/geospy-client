(function() {
'use strict';

  angular
    .module('PermissionModule', [])
    .factory('PermissionService', PermissionService);

  PermissionService.$inject = ['ConfigService','PermRoleStore','PermPermissionStore','roleName','permissionName'];
  function PermissionService(ConfigService, PermRoleStore, PermPermissionStore, roleName, permissionName) {
    var service = {
      definePermission: definePermission
    };
    
    return service;
    
    function definePermission() {
      setPermission();
      setRole();
    }

    function setPermission() {
      PermPermissionStore.definePermission(permissionName.seeDashboard, function() { return true; });
    }

    function setRole() {
      PermRoleStore.defineRole(roleName.AUTHORIZED, authorizedRole);
      PermRoleStore.defineRole(roleName.ADMIN, [permissionName.seeDashboard]);  
      PermRoleStore.defineRole(roleName.USER, [permissionName.seeDashboard]);  
    }

    function authorizedRole() {
      let currentUser = ConfigService.getProfile(); 
      return currentUser ?  currentUser.isLogged : false;
    }

  }
})();