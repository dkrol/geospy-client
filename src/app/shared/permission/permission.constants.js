(function() {
  'use strict';

  angular
    .module('PermissionModule')
    .constant('roleName', {
      GUEST: 'GUEST',
      USER: 'USER',
      ADMIN: 'ADMIN',
      AUTHORIZED: 'AUTHORIZED'
    })
    .constant('permissionName', {
      seeDashboard: 'seeDashboard'
    });

})();
