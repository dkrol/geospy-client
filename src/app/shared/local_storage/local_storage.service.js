(function () {
  'use strict';

  angular
    .module('LocalStorageModule', [])
    .factory('LocalStorageService', LocalStorageService);

  LocalStorageService.$inject = ['$window'];
  function LocalStorageService($window) {
    return {
      setItem: setItem,
      getItem: getItem
    };

    function setItem(name, data) {
      // $window.localStorage.setItem(name,JSON.stringify(data));
      $window.localStorage.setItem(name, angular.fromJson(data));
    }

    function getItem(name) {
      // let result = JSON.parse($window.localStorage.getItem(name));
      return angular.toJson($window.localStorage.getItem(name));
    }

  }
})();