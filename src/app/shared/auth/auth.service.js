(function() {
'use strict';

  angular
    .module('AuthServiceModule', [])
    .factory('AuthService', AuthService);

  AuthService.$inject = ['$http','ConfigService'];
  function AuthService($http, ConfigService) {
    let service = {
      getHeaders: getHeaders
    };
    
    return service;
    
    function getHeaders() {
      let currentUser = ConfigService.getProfile();
      let headers = { Authorization: 'Bearer ' + currentUser.token };
      return headers;
    }
  }
})();