(function() {
'use strict';

  angular
    .module('NotificationModule', [])
    .factory('NotificationService', NotificationService);

  NotificationService.$inject = ['Notification'];
  function NotificationService(Notification) {
    var service = {
      primary: primary,
      success: success,
      warning: warning,
      error: error,
      showResponseErrors: showResponseErrors
    };
    
    return service;

    function primary(message) {
      Notification.primary(message);
    }

    function success(message) {
      Notification.success(message);
    }

    function warning(message) {
      Notification.warning(message);
    }

    function error(message) {
      Notification.error(message);
    }

    // TODO: Dopisać obsluge dla response.data.excceptionMessage oraz response.data.error

    function showResponseErrors(response) {
      if (response.data && response.data.modelState) {
        for (let key in response.data.modelState) {
          let errorArray = response.data.modelState[key];
          for (let i = 0, length = errorArray.length; i < length; i++) {
            Notification.error(errorArray[i]);
          }
        }
      }
    }

  }
})();