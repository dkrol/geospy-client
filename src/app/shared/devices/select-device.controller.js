/*eslint angular/controller-as: 2*/
(function () {
  'use strict';

  angular
    .module('SelectDeviceDirectiveModule')
    .controller('SelectDeviceController', SelectDeviceController);

  SelectDeviceController.inject = ['DevicesService', 'ConfigService', '$scope', 'Notification'];
  function SelectDeviceController(DevicesService, ConfigService, $scope, Notification) {
    var vm = this;
    vm.scope = $scope;

    vm.devices = null;
    vm.scope.selected = null;

    initDevices();

    function initDevices() {
      let user = ConfigService.getProfile();
      DevicesService.getDevicesByUser(user)
        .then(getDevicesSuccess)
        .catch(() => { Notification.error('Error retrieving data'); });

      function getDevicesSuccess(response) {
        if (response.data) {
          vm.devices = response.data;
        }
      }
    }

  }
})();