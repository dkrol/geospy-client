(function() {
'use strict';

  angular
    .module('DevicesServiceModule', [])
    .factory('DevicesService', DevicesService);

  DevicesService.$inject = ['$http','urlsConst','AuthService'];
  function DevicesService($http, urlsConst, AuthService) {
    let headers = AuthService.getHeaders();
    let apiUrl = urlsConst.API;
    let service = {
      getDevicesByUser: getDevicesByUser
    };
    
    return service;

    function getDevicesByUser(data) {
      let url = apiUrl + '/api/devices/user';
      return $http({
        method: 'POST',
        url: url,
        data: data,
        headers: headers
      });
    }
  }
})();