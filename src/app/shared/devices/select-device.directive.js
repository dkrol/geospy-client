(function() {
  'use strict';

  angular
    .module('SelectDeviceDirectiveModule', [])
    .directive('selectDevice', selectDevice);

  function selectDevice() {
    var directive = {
        templateUrl: 'app/shared/devices/select-device.view.html',
        controller: 'SelectDeviceController',
        controllerAs: 'selectDevCtrl',
        restrict: 'E',
        scope: {
          selected: '='
        }
    };
    return directive;
  }
})();