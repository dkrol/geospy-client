(function() {
'use strict';

  angular
    .module('HelpersModule', [])
    .factory('ParseHelperService', ParseHelperService);

  function ParseHelperService() {
    var service = {
      getDateTimeFromString: getDateTimeFromString,
      getStringFromDateTime: getStringFromDateTime
    };
    
    return service;


    function getDateTimeFromString(timestamp) {
      let timestampArray = timestamp.split(' ');
      let date = timestampArray[0].split('/');
      let time = timestampArray[1].split(':');
      let datetime = new Date(date[2], date[1] - 1, date[0], time[0], time[1], time[2], 0);
      
      return datetime;
    }

    function getStringFromDateTime(datetime) {
      let date = datetime.date.getDate();
      let month = datetime.date.getMonth() + 1;
      let year = datetime.date.getFullYear();
      let hour = datetime.time.getHours();
      let minute = datetime.time.getMinutes();
      let second = datetime.time.getSeconds();
      let result = setZero(date) + '/' + setZero(month) + '/' + setZero(year) + ' ' + setZero(hour) + ':' + setZero(minute) + ':' + setZero(second);
      return result;
    }

    function setZero(value) {
      if (value >= 0 && value <= 9) {
        return value == 0 ? '00' : '0' + value;
      }
      return value;
    }
  }
})();