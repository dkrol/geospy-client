(function() {
  'use strict';

  angular
    .module('GeoSpy')
    .config(routerConfig);

  function routerConfig($stateProvider, $urlRouterProvider, roleName) {
    $stateProvider
      .state('signin', {
        url: '/',
        views: {
          'loginView': {
            templateUrl: 'app/login/signin/signin.view.html',
            controller: 'SigninController',
            controllerAs: 'signinCtrl',
            data: {
              title: 'Signin'
            }
          }
        },
        data: {
          permissions: {
            except: [roleName.AUTHORIZED],
            redirectTo: 'dashboard'
          }
        }
      })
      .state('dashboard', {
        url: '/dashboard',
        views: {
          'bodyView': {
            templateUrl: 'app/dashboard/dashboard.view.html',
            controller: 'DashboardController',
            controllerAs: 'dashbCtrl',
            data: {
              title: 'Dashboard'
            }
          }
        },
        data: {
          permissions: {
            only: ['AUTHORIZED'],
            redirectTo: 'signin'
          }
        }
      })
      .state('registration', {
        url: '/registration',
        views: {
          'loginView': {
            templateUrl: 'app/login/registration/registration.view.html',
            controller: 'RegistrationController',
            controllerAs: 'regCtrl',
            data: {
              title: 'Registration'
            }
          }
        },
        data: {
          permissions: {
            except: ['AUTHORIZED'],
            redirectTo: 'dashboard'
          }
        }
      })
      .state('devices', {
        url: '/devices',
        views: {
          'bodyView': {
            templateUrl: 'app/devices/devices.view.html',
            controller: 'DevicesController',
            controllerAs: 'devCtrl',
            data: {
              title: 'Devices'
            }
          }
        },
        data: {
          permissions: {
            only: ['AUTHORIZED'],
            redirectTo: 'signin'
          }
        }
      })
      .state('tracking', {
        url: '/tracking',
        views: {
          'bodyView': {
            templateUrl: 'app/tracking/tracking.view.html',
            controller: 'TrackingController',
            controllerAs: 'trackCtrl',
            data: {
              title: 'Tracking'
            }
          }
        },
        data: {
          permissions: {
            only: ['AUTHORIZED'],
            redirectTo: 'signin'
          }
        }
      })
      .state('location', {
        url: '/location',
        views: {
          'bodyView': {
            templateUrl: 'app/location/location.view.html',
            controller: 'LocationController',
            controllerAs: 'locCtrl',
            data: {
              title: 'Location'
            }
          }
        },
        data: {
          permissions: {
            only: ['AUTHORIZED'],
            redirectTo: 'signin'
          }
        }
      });
      // .state('users', {
      //   url: '/users',
      //   views: {
      //     'bodyView': {
      //       templateUrl: 'app/users/users.view.html',
      //       controller: 'UsersController',
      //       controllerAs: 'usrCtrl',
      //       data: {
      //         title: 'Users'
      //       }
      //     }
      //   },
      //   data: {
      //     permissions: {
      //       only: ['AUTHORIZED'],
      //       redirectTo: 'signin'
      //     }
      //   }
      // });
      
    $urlRouterProvider.otherwise('/');
  }

})();
