(function() {
  'use strict';

  angular
    .module('GeoSpy')
    .constant('urlsConst', {
      API: 'http://localhost:5001/api'
      // API: 'http://192.168.137.1:28424',
      // API_LOCAL: 'http://localhost:28424',
      // API_IIS: 'http://192.168.0.101:9810'
    
    })
    .constant('configConst', {
      CURRENT_USER: 'currentUser'
    });

})();
