(function() {
'use strict';

  angular
    .module('ConfigModule', [])
    .factory('ConfigService', ConfigService);

  ConfigService.$inject = ['LocalStorageService', 'configConst'];
  function ConfigService(LocalStorageService, configConst) {
    return {
      setProfile: setProfile,
      getProfile: getProfile,
      clearProfile: clearProfile
    };

    function setProfile(currentUser) {
      LocalStorageService.setItem(configConst.CURRENT_USER, currentUser);
    }

    function getProfile() {
      return LocalStorageService.getItem(configConst.CURRENT_USER);
    }

    function clearProfile() {
      LocalStorageService.setItem(configConst.CURRENT_USER, '');
    }
  }
})();