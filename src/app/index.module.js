(function() {
  'use strict';

  angular
    .module('GeoSpy', [
      'ngAnimate', 
      'ngCookies', 
      'ngMessages', 
      'ui.router', 
      'ui.bootstrap', 
      'toastr', 
      'ngMaterial', 
      'ngResource',
      'ui-notification',
      'permission',
      'permission.ui',
      'LocalStorageModule',
      'ConfigModule',
      'PermissionModule',
      'NotificationModule',
      
      'ngMap',
      'ngTable',
      
      'AuthServiceModule',
      'HelpersModule',
      'DatetimePickerModule',
      'DevicesServiceModule',
      'SelectDeviceDirectiveModule',

      'MainModule',
      'LoginModule',
      'SigninModule',
      'RegistrationModule',
      'HeaderModule',
      'DashboardModule',
      'UsersModule',
      'DevicesModule',
      'LocationModule',
      'TrackingModule'
      ]);
})();
